const data = [

    {
      "id": 2,
      "name": "Parents",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },
    {
      "id": 11,
      "name": "Brother - in - Law",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 6,
      "name": "Nephew",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 1,
      "name": "Husband",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 0,
      "name": "Niece",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },
    {
      "id": 9,
      "name": "Grandfather",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 7,
      "name": "Uncle",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 30,
      "name": "Wife",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },
    {
      "id": 8,
      "name": "Siblings",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 12,
      "name": "Son",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 10,
      "name": "Aunt",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },
    {
      "id": 5,
      "name": "Children",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },
    {
      "id": 3,
      "name": "Grandmother",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },
    {
      "id": 13,
      "name": "Grandson",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },
    {
      "id": 15,
      "name": "Grandchildren",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 14,
      "name": "Mother",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },

    {
      "id": 17,
      "name": "Stepsisters",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },
    {
      "id": 16,
      "name": "Daughter",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },

    {
      "id": 19,
      "name": "Stepfather",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },

    {
      "id": 20,
      "name": "Stepmother",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },

    {
      "id": 25,
      "name": "Grandparents",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 26,
      "name": "Relatives",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 22,
      "name": "Granddaughter",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },

    {
      "id": 18,
      "name": "Friends",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 27,
      "name": "Mother - in - Law",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    },

    {
      "id": 21,
      "name": "Cousins",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 29,
      "name": "Stepbrothers",
      "audio": "media/audio.mp3",
      "areaTarget": "area_1"
    },

    {
      "id": 23,
      "name": "Father",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },

    {
      "id": 28,
      "name": "Father-in-law",
      "audio": "media/audio.mp3",
      "areaTarget": "area_3"
    },

    {
      "id": 24,
      "name": "Sister-in-law",
      "audio": "media/audio.mp3",
      "areaTarget": "area_2"
    }

  ]


export default data